const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const orderController = require("../controllers/orderController.js")


// Create Order/Checkout (Non-admin)
router.get("/checkout", auth.verify, (req,res)=>{
  const userData = auth.decode(req.headers.authorization)
  orderController.checkout(req.body, {userId: userData.id}).then(result=>res.send(result))
})

// find authenticated user's order/s
router.get("/checkOrder", auth.verify, (req,res)=>{
  const userData = auth.decode(req.headers.authorization)
  orderController.checkOrder({userId: userData.id}).then(result=>res.send(result))
})

// show all orders (admin only)
router.get("/showAllOrders", auth.verify, (req,res)=>{
  const userData = auth.decode(req.headers.authorization)
  orderController.showAllOrders({userId: userData.id}).then(result=>res.send(result))
})
module.exports = router