const User = require("../models/user.js")
const Product = require("../models/product.js")
const Order = require("../models/order.js")

// checkout
module.exports.checkout= async(reqBody, data)=>{

  let isOrderUpdated = await User.findById(data.userId).then((result,err)=>{
    if (err){
      console.log(err)
      return false
    }else{
      if (result === null){
        return false
      }else if(result.isAdmin === true){
        return "You are an admin. You can not order."
      }else{
        return Product.findById(reqBody.productId).then((buyProduct, err)=>{
          if (err){
            console.log(err)
            return false
          }else{
            let newOrder = new Order({
              totalAmount: buyProduct.price,
              orderDetails:{
                userId: data.userId,
                productId: reqBody.productId
              }
            })
            return newOrder.save().then((saved, err)=>{
              if (err){
                console.log(err)
              }else{
                return true
              }
            })
          }
        })

      }
    }
  })
  let isUserUpdated = await User.findById(data.userId).then(user=>{
    
    user.myOrder.push({productId: reqBody.productId})
   
    return user.save().then((user, err)=>{
      if(err){
        return false
      }else{
        return true
      }
    })
  })
  if (isOrderUpdated && isUserUpdated){
    return "New Order has been made"
  }else{
    return false
  }
}

// authenticated user's order
module.exports.checkOrder = (data) => {
  return User.findById(data.userId).then((user, err)=>{
    if(err){
      return false
    }else{
      if (user === null){
        return false
      }else if(user.isAdmin === true){
        return "You are an admin"
      }else{
        return Order.find({userId:data.userId}).then((result, err)=>{
          if(err){
            return false
          }else{
            return result
          }
        })
      }
    }
  })
}

// Retrieve all orders (Admin only)
module.exports.showAllOrders = (data) => {
  return User.findById(data.userId).then((user, err)=>{
    if(err){
      return false
    }else{
      if (user === null){
        return false
      }else if(user.isAdmin === false){
        return "You are not an admin"
      }else{
        return Order.find().then((result, err)=>{
          if(err){
            return false
          }else{
            return result
          }
        })
      }
    }
  })
}