const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const productController = require("../controllers/productController.js")

// Create Product (Admin only)
router.post("/addProduct", auth.verify, (req,res)=>{
  const userData = auth.decode(req.headers.authorization)
  productController.addProduct(req.body, {userId: userData.id}).then(resultFromController=>res.send(resultFromController))
})

// retrieve all active products
router.get("/activeProducts", (req, res)=>{
  productController.findActiveProducts().then(result=>res.send(result))
})

// retrieve a single product
router.get("/:productId", (req, res)=>{
  productController.findProduct(req.params).then(resultFromController=>res.send(resultFromController))
})

// Update product information
router.put("/:productId", auth.verify, (req,res)=>{
  const userData = auth.decode(req.headers.authorization)
  productController.updateProduct(req.params, req.body, {userId: userData.id}).then(resultFromController=>res.send(resultFromController))
})

// Archive a product
router.put("/archive/:productId", auth.verify, (req,res)=>{
  const userData = auth.decode(req.headers.authorization)
  productController.archiveProduct(req.params, req.body, {userId: userData.id}).then(resultFromController=>res.send(resultFromController))
})









module.exports = router