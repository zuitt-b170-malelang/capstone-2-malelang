const User = require("../models/user.js")
const Product = require("../models/product.js")
const Order = require("../models/order.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt")
const { model } = require("mongoose")
const user = require("../models/user.js")

// create user
module.exports.registerUser = (reqBody) => {
  let newUser = new User({
    name: reqBody.name,
    email: reqBody.email,
    password: bcrypt.hashSync(reqBody.password, 10),
    mobileNo: reqBody.mobileNo,
    address: reqBody.address
  })
  return newUser.save().then((saved, err)=>{
    if(err){
      console.log(err)
      return false
    }else{
      return "User Registration Successful"
    }
  })
}

// login
module.exports.userLogin = (reqBody) => {
  return User.findOne({email: reqBody.email}).then(result=>{
    if(result === null){
      return false
    }else{

      const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

      if(isPasswordCorrect){
        return {access: auth.createAccessToken(result.toObject())}
      }
    }
  })
}

module.exports.setAsAdmin = (reqParams, data) =>{
  let updateIsAdmin = {
    isAdmin: true
  }
  return User.findById(data.userId).then((result, err)=>{
    if(err){
      console.log(err)
      return false
    }else{
      if(result === null){
        return false
      }else if(result.isAdmin === false){
        return "You are not an admin"
      }else{
        return User.findByIdAndUpdate(reqParams.userId, updateIsAdmin).then((result, err)=>{
          if(err){
            return false
          }else{
            return "User is now an admin"
          }
        })
      }
    }
  })
}

// find all users
module.exports.getAllUsers=()=>{
  return User.find({}).then((result, err)=>{
    if(err){
      return false
    }else{
      return result
    }
  })
}

// Add product to tray
module.exports.addToTray = (data) => {

  return User.findByIdAndUpdate(data.userId, ).then(user=>{
    return Product.findById(data.productId).then(result=>{
      user.tray.push({productId: data.productId})
      return user.save().then((user, err)=>{
        if(err){
          return false
        }else{
          return true
        }
      })
    })
  })
}






 /*  return Product.findById(data.productId).then(result=>{
    console.log(result)
    let addProduct = {
      tray:[{
        productId: result.productId,
        price: result.price
      }]
    }
    return User.findByIdAndUpdate(data.userId, addProduct).then((saved, err)=>{
      if(err){
        return false
      }else{
        return "Product added to Tray"
      }
    })
  })
} */