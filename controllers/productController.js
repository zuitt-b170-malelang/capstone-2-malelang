const User = require("../models/user.js")
const Product = require("../models/product.js")
const Order = require("../models/order.js")

// create a product
module.exports.addProduct = (reqBody, data) => {
  return User.findById(data.userId).then((result, err)=>{
    if(err){
      console.log(err)
      return false
    }else{
      if(result === null){
        return false
      }else if(result.isAdmin === false){
        return "You are not an admin"
      }else{
        let newProduct = new Product({
          name: reqBody.name,
          description: reqBody.description,
          price: reqBody.price,
          category: reqBody.category
        })
        return newProduct.save().then((saved, err)=>{
          if(err){
            console.log(err)
            return false
          }else{
            return "A product has been created"
          }
        })
      }
    }
  })
}

// retrieve all active products
module.exports.findActiveProducts = () => {
  return Product.find({}).then((result, err)=>{
    if (err){
      console.log(err)
      return false
    }else{
      return(result)
    }
  })
}

// find a single product
module.exports.findProduct = (reqParams) => {
  return Product.findById(reqParams.productId).then((result, err)=>{
    if(err){
      console.log(err)
      return false
    }else{
      return(result)
    }
  })
}

// find and update a product
module.exports.updateProduct = (reqParams, reqBody, data) =>{
  let updatedProduct = {
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price
  }
  return User.findById(data.userId).then((result, err)=>{
    if(err){
      console.log(err)
      return false
    }else{
      if(result === null){
        return false
      }else if(result.isAdmin === false){
        return "You are not an admin"
      }else{
        return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((result, err)=>{
          if (err){
            console.log(err)
            return false
          }else{
            return "Product has been updated"
          }
        })
      }
    }
  })
}

// archive a product (admin only)
module.exports.archiveProduct = (reqParams, reqBody, data) =>{

  let updateProduct = {
    isAvailable: reqBody.isActive
  }
  return User.findById(data.userId).then((result, error)=>{
    if(error){
      return false
    }else{
      if(result === null){
        return false
      }else if(result.isAdmin === false){
        return "You are not an admin"
      }else{
        return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((result, error)=>{
          if(error){
            return false
          }
          else{
            return "Product is now unavailable"
          }
      })

      }
    }
  })
}