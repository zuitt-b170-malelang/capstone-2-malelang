const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Course name is required"]
  },
  description: {
    type: String,
    required: [true, "Course description is required"]
  },
  price:{
    type: Number,
    required: [true, "Price is required"]
  },
  isAvailable:{
    type: Boolean,
    default: true
  },
  category:[
     {
     type: String,
     required: [true, "Category of product is required"]
    }
  ],
  createdOn:{
    type: Date,
    default: new Date()
  }
})

module.exports = mongoose.model("Product", productSchema)