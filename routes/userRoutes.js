const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const userController = require("../controllers/userController.js")

// register user
router.post("/register", (req,res)=>{
  userController.registerUser(req.body).then(result=> res.send(result))
})

// login
router.post("/login", (req, res)=>{
  userController.userLogin(req.body).then(result=>res.send(result))
})

// make a user an admin
router.put("/setAsAdmin/:userId", auth.verify, (req,res)=>{
  const userData = auth.decode(req.headers.authorization)
  userController.setAsAdmin(req.params, {userId: userData.id}).then(result=> res.send(result))
})

// find all users
router.get("/allUsers", (req,res)=>{
  userController.getAllUsers().then(result=>res.send(result))
})

// add to tray
router.post("/addToTray", auth.verify, (req, res)=>{
  let data = {
    userId: auth.decode(req.headers.authorization).id,
    productId: req.body.productId
  }
  userController.addToTray(data).then(resultFromController=>res.send(resultFromController))
})








module.exports = router