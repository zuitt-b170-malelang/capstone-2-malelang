const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "A name is required"]
  },
  
  email:{
    type:String,
    required: [true, "Email is required"]
  },
  password:{
    type:String,
    required: [true, "Password is required"]
  },
  mobileNo:{
    type:String,
    required: [true, "Mobile Number is required"]
  },
  address:{
    type:String,
    required: [true, "Address is required"]
  },
  isAdmin:{
    type:Boolean,
    default: false
  },
  myOrder: [{
    productId: {
      type: String,
      required: [true, "Order ID is required"]
    },
    purchasedOn: {
      type: Date,
      default: new Date()
    }
  }],
  tray: [
    {
      productId: {
        type:String,
        required: [true, "Product ID is required"]
      },
      addedOn: {
        type: Date,
        default: new Date()
      },
      status:{
        type: String,
        default: "Pending"
      }
    }
  ]

})

module.exports = mongoose.model("User", userSchema)