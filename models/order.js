const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
  totalAmount: {
    type: Number,
    required: [true, "Course name is required"]
  },
  purchasedOn: {
    type: Date,
    default: new Date()
  },
  orderDetails: 
    {
      userId: {
        type: String,
        required: [true, "User ID is required"]
      },
      productId: 
        [{
          type: String,
          required: [true, "User ID is required"]
        }]
      
    }
  
})

module.exports = mongoose.model("Orders", orderSchema)